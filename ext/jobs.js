var job_cache = {};
var active_jobs = {};
module.exports = [{
    'event': 'command',
    'name': 'jobs',
    'description': 'browse the job market',
    'execute': function(ctx, args, msg) {
        var base = 10,
            radius = 5;
        if (active_jobs[msg.author.id]) {
            //
            msg.channel.createMessage("You're already doing a job! You are currently " + active_jobs[msg.author.id].text + ".");
        }
        else if (!job_cache[msg.author.id]) {
            job_cache[msg.author.id] = [];
            for (var i = 0; i < 5; i++) {
                job_cache[msg.author.id].push(gen_job(base, radius));
            }
            msg.channel.createMessage("Available jobs:\n" + get_string(msg.author.id));
        }
        else {
            var c = args.split(" ")
            if (parseInt(c[0]) && parseInt(c[0]) - 1 < job_cache[msg.author.id].length) {
                active_jobs[msg.author.id] = job_cache[msg.author.id][parseInt(c[0]) - 1];
                delete job_cache[msg.author.id];
                msg.channel.createMessage("Okay! You are now " + active_jobs[msg.author.id].text + " for the next " + active_jobs[msg.author.id].time + " minutes.");
                setTimeout(() => {
                    add_job_bal(msg, active_jobs[msg.author.id].value).then(() => {
                        delete active_jobs[msg.author.id];
                    });
                }, (active_jobs[msg.author.id].time * 60 * 1000));
            }
            else {
                msg.channel.createMessage("Invalid arguments. Please specify a number from 1 to " + job_cache[msg.author.id].length + ". \nAvailable jobs:\n" + get_string(msg.author.id));
            }
        }
    }
}];
var first_half_choices = ["harvesting potatoes", "collecting legos", "picking cacti", "helping old ladies across the street", "containing fireballs", "strip mining", "writing squiggly lines", "collecting money on the side of the road", "driving a taxi", "strip mining", "picking weeds", "chopping trees", "filing away complaints", "cryptomining for bitcoins", "hacking into the mainframe", "starting a pyramid scheme", "attending jury duty", "conning clueless tourists"];
var second_half_choices = ["in an empty field", "without any tools", "while barefoot", "in the middle of a desert, without any water", "wearing a blindfold", "in the smoldering heat", "in the blistering cold", "in the depths of the night", "after getting doused in water", "while being chased by animals", "with the police on your trail", "while being tracked", "with greasy hands", "near a large church", "in a state of constant anxiety", "while wondering if you left the stove on"];
async function add_job_bal(msg, increment) {
    var inv = await ctx.inventory._get_inventory(msg.author);
    var item_amt = await inv.get_balance();
    msg.channel.createMessage("Congratulations, <@" + msg.author.id + ">! You are now " + increment + "💰 richer!");
    inv.set_balance(item_amt + increment);
}

function gen_job(base, radius) {
    var ret = {};
    var half = radius / 2;
    var half_base = base - half;
    var rand = Math.floor(Math.random() * radius);
    var rand2 = Math.floor(Math.random() * radius);
    ret.value = (half_base + rand) * 2;
    ret.time = (half_base / 2) + Math.pow(rand2, 2);
    //
    var str = first_half_choices[Math.floor(Math.random() * first_half_choices.length)] + " " + second_half_choices[Math.floor(Math.random() * second_half_choices.length)];
    ret.text = str;
    return ret;
}

function get_string(id) {
    var list = job_cache[id];
    var str = "";
    for (var job in list) {
        str += list[job].text + "\t" + list[job].value + "💰 for " + list[job].time + " minutes.\n";
    }
    return str;
}