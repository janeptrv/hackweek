var items = {
    rose: { shopName: "Rose Seed", plantName: "Rose", icon: "🌹", cost: 5, time: 2, sellValue: 7 },
    tulip: { shopName: "Tulip Seed", plantName:"Tulip", icon: "🌷", cost: 7, time: 4, sellValue: 10 },
    shamrock: { shopName: "Four Leaved Clover Seed", plantName:"Four Leaved Clover", icon: "🍀", cost: 15, time: 10, sellValue: 30 },
    redapple: { shopName: "Red Apple Seed", plantName:"Red Apple", icon: "🍎", cost: 25, time: 20, sellValue: 50 },
    greenapple: { shopName: "Green Apple Seed", plantName:"Green Apple", icon: "🍏", cost: 50, time: 45, sellValue: 100 },
    carrot: { shopName: "Carrot Seed", plantName:"Carrot", icon: "🥕", cost: 10, time: 3, sellValue: 20 },
    //When the hibiscus grows is when stuff starts to turn bad
    hibiscus: { shopName: "Hibiscus Seed", plantName:"Hibiscus", icon: "🌺", cost: 20, corruption: 4, time: 4, sellValue: 10 },
    anthive: { shopName: "Ant", icon: "🐜", plantName:"Anthive", cost: 20, corruption: 10, threshold : 40, time: 8, sellValue: 90 },
    badrose: { shopName: "Cursed Rose Seed", plantName:"Cursed Rose", icon: "🥀",corruption: 30, threshold : 60, cost: 15, time: 4, sellValue: 50 }
}
ctx._item_codex = items;
module.exports = [{
    'event': 'command',
    'name': 'shop',
    'execute': async function(ctx, args, msg) {
        var text = "";
        for (var i in items) {
            text += (items[i].icon + " " + items[i].shopName + "\t" + items[i].cost + "💰" + "\n");
        }
        msg.channel.createMessage(text);
    }
}, {
    'event': 'command',
    'name': 'buy',
    'execute': async function(ctx, args, msg) {
        var c = args.split(" ");
        var a = c[0];
        if (c[1]) a = c[0] + c[1];
        a = a.toLowerCase();
        if (items[a]) {
            var inv = await ctx.inventory._get_inventory(msg.author);
            var item_amt = await inv.get_item_amount(items[a].id);
            var bal = await inv.get_balance();
            if (bal >= items[a].cost) {
                msg.channel.createMessage("Thanks for buying " + items[a].icon + " at the low cost of " + items[a].cost + "!");
                inv.set_item(a, item_amt + 1);
                inv.set_balance(bal - items[a].cost);
                if (items[a].corruption) inv.increment_corruption(items[a].corruption);
            }
            else {
                msg.channel.createMessage("Sorry, you do not have enough money for " + items.rose.icon + " .");
            }
        }
        else {
            msg.channel.createMessage("Sorry, that is invalid.");
        }
    }
}, {
    'event': 'command',
    'name': 'sell',
    'execute': async function(ctx, args, msg) {
        if (items[a]) {
            var inv = await ctx.inventory._get_inventory(msg.author);
            var item_amt = await inv.get_item_amount(items[a].id);
            var bal = await inv.get_balance();
            if (item_amt >= 1) {
                msg.channel.createMessage("Pleasure doin business with you. Here's " + items[a].sellValue + " .");
                inv.set_item(a, item_amt - 1);
                inv.set_balance(bal + items[a].sellValue);
            }
            else {
                msg.channel.createMessage("Sorry, you don't have that.");
            }
        }
        else {
            msg.channel.createMessage("Sorry, that is invalid.");
        }
    }
}, {
    'event': 'command',
    'name': 'balance',
    'execute': async function(ctx, args, msg) {
        var inv = await ctx.inventory._get_inventory(msg.author);
        var bal = await inv.get_balance();
        msg.channel.createMessage("Your current balance is " + bal + ".");
    }
}, {
    'event': 'command',
    'name': 'inventory',
    'execute': async function(ctx, args, msg) {
        const PAGE_ITEM_LIMIT = 10;
        var page = parseInt(args.split(" ")[0]) - 1 || 0;
        var inv_obj = await ctx.inventory._get_inventory(msg.author);
        var inv_cont = await inv_obj.get_inventory();
        var pages = {};
        var cpg = 0;
        var ic = 0;
        for (var item in inv_cont) {
            if (item != '_cache_timeout') {
                ic++;
                if (ic != 0 && ic % PAGE_ITEM_LIMIT == 0) {
                    cpg++;
                }
                if (!pages[cpg]) pages[cpg] = [];
                pages[cpg].push(items[item].icon + " " + items[item].shopName + "\t" + inv_cont[item]);
            }
        }
        var str = "";
        for (var item in pages[page]) {
            str += pages[page][item] + "\n";
        }
        if (pages[page + 1]) {
            str += "\n Page " + (page + 1) + " is available!";
        }
        msg.channel.createMessage("Inventory (Page " + (page + 1) + "):\n" + str);
    }
}]