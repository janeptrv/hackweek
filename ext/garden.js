var mark = "^ABCDEFGHIJKLMNOPQRSTUVWXYZ";
ctx._coord_decode = mark;
module.exports = [{
    'event': 'command',
    'name': 'garden',
    'execute': async function(ctx, args, msg) {
        var inv = await ctx.inventory._get_inventory(msg.author);
        var garden = await inv.get_garden();
        var size = garden._size;
        if (args) {
            var c = args.split(" ");
            var items = ctx._item_codex;
            switch (c[0]) {
                case "plant":
                    if (verify_coords(size, c[1].toUpperCase(), garden) && items[c[2]]) {
                        var item_amt = await inv.get_item_amount(c[2]);
                        console.log(item_amt);
                        if (item_amt > 0) {
                            msg.channel.createMessage("OK! Check your garden again to see the new configuration!");
                            inv.set_item(c[2], item_amt - 1);
                            inv.plant(c[1], c[2], 0);
                        }
                        else {
                            msg.channel.createMessage("You don't have any of those.");
                        }
                    }
                    else {
                        if (!verify_coords(size, c[1].toUpperCase(), garden)) {
                            syntax(msg, "coordinate verify failed.");
                        }
                        else if (!items[c[2]]) {
                            syntax(msg, "item does not exist.");
                        }
                        else {
                            syntax(msg);
                        }
                    }
                    break;
                case "harvest":
                case "remove":
                    if (verify_coords(size, c[1].toUpperCase(), garden, true)) {
                        msg.channel.createMessage("OK! Check your garden again to see the new configuration!");
                        inv.unplant(c[1]);
                    }
                    else {
                        syntax(msg, "coordinate verify failed. perhaps there's nothing there?");
                    }
                    break;
            }
        }
        else {
            console.log(garden);
            var items = ctx._item_codex;
            var str = "";
            for (var i = 0; i <= size; i++) {
                str += mark[i] + " ";
                for (var j = 1; j <= size; j++) {
                    if (i == 0) {
                        str += "   " + j + "  ";
                    }
                    else if (garden[mark[i]] && garden[mark[i]][j]) {
                        str += items[garden[mark[i]][j].id].icon;
                    }
                    else {
                        console.log(i, mark[i], garden[mark[i]], garden[mark[i]] && garden[mark[i]][j]);
                        str += "☘";
                    }
                }
                str += "\n";
            }
            console.log(str);
            msg.channel.createMessage(str);
        }
    }
}];

function verify_coords(size, coordinate, garden, needsExist = false) {
    if (!coordinate || !garden || !size) return false;
    if (needsExist && (!garden[coordinate[0]] || !garden[coordinate[0]][coordinate[1]])) return false;
    var idex = mark.indexOf(coordinate[0]);
    var idex2 = coordinate[1];
    if (idex == -1) return false;
    if (idex <= size && idex2 <= size) return true;
}

function syntax(msg, addon) {
    msg.channel.createMessage((addon ? addon + "\n" : "") + "Syntax:\ngarden <plant/harvest> <coordinate> <item name>");
}