ctx.inventory = {};
ctx.inventory._inventories = []
var db = ctx.sql._db;
var sequelize = ctx.sql._sequelize;
const Users_Schema = db.define('user', {
    uid: {
        type: sequelize.STRING,
        unique: true,
        primaryKey: true
    },
    balance: {
        type: sequelize.DOUBLE,
        defaultValue: 0.0
    },
    corruption: {
        type: sequelize.DOUBLE,
        defaultValue: 0.0
    },
    garden_size: {
        type: sequelize.INTEGER,
        defaultValue: 3
    },
    permission: {
        type: sequelize.INTEGER,
        defaultValue: 0
    }
});
const Stats_Schema = db.define('stats', {
    uid: {
        type: sequelize.STRING,
        primaryKey: true
    },
    stat_name: {
        type: sequelize.STRING
    },
    count: {
        type: sequelize.INTEGER,
        defaultValue: 0
    }
});
const Inventory_Schema = db.define('inventory', {
    uid: {
        type: sequelize.STRING,
        primaryKey: true
    },
    item_name: {
        type: sequelize.STRING
    },
    amount: {
        type: sequelize.INTEGER,
        defaultValue: 0
    }
});
const Garden_Schema = db.define('garden', {
    uid: {
        type: sequelize.STRING,
        primaryKey: true
    },
    item_name: {
        type: sequelize.STRING
    },
    coordinate: {
        type: sequelize.STRING
    },
    growth: {
        type: sequelize.INTEGER,
        defaultValue: 0
    }
});
class Inventory {
    constructor(user) {
        this.id = user.id;
        this.cached_inventory = {};
        this.cached_garden = {};
        ctx.inventory._inventories.push(this);
    }
    async get_inventory() {
        Inventory_Schema.sync();
        var inv = {};
        if (this.cached_inventory) {
            if (this.cached_inventory._cache_timeout) {
                this.cached_inventory._cache_timeout--;
                return this.cached_inventory;
            }
            else {
                this.cached_inventory = {};
            }
        }
        var user_inventory = await db.models.inventory.findAll({
            where: {
                uid: this.id
            }
        });
        for (var item_index in user_inventory) {
            var item = user_inventory[item_index];
            if (item && item.dataValues && item.dataValues.amount > 0) {
                inv[item.dataValues.item_name] = item.dataValues.amount;
            }
        }
        if (inv) {
            this.cached_inventory = inv;
            this.cached_inventory._cache_timeout = ctx.settings.cache_timeout || 3;
        }
        return inv;
    }
    async get_garden() {
        Garden_Schema.sync();
        var inv = {};
        if (this.cached_garden) {
            if (this.cached_garden._cache_timeout) {
                this.cached_garden._cache_timeout--;
                return this.cached_garden;
            }
            else {
                this.cached_garden = {};
            }
        }
        var user_garden = await db.models.garden.findAll({
            where: {
                uid: this.id
            }
        });
        for (var item_index in user_garden) {
            var item = user_garden[item_index];
            if (item && item.dataValues) {
                if (!inv[item.dataValues.coordinate[0]]) inv[item.dataValues.coordinate[0]] = {};
                inv[item.dataValues.coordinate[0]][item.dataValues.coordinate[1]] = { id: item.dataValues.item_name, growth: item.dataValues.growth };
            }
        }
        var user = await db.models.user.findOrCreate({
            where: {
                uid: this.id
            },
            defaults: {
                uid: this.id
            }
        });
        user = user[0];
        inv._size = user.dataValues && user.dataValues.garden_size ? user.dataValues.garden_size : 3;
        if (inv) {
            this.cached_garden = inv;
            this.cached_garden._cache_timeout = ctx.settings.cache_timeout || 3;
        }
        return inv;
    }
    async plant(coordinate_, item_id, growth_) {
        await db.models.garden.findOrCreate({
            where: {
                uid: this.id,
                coordinate: coordinate_
            },
            defaults: {
                uid: this.id,
                coordinate: coordinate_
            }
        });
        await db.models.garden.update({
            item_name: item_id
        }, {
            where: {
                uid: this.id,
                coordinate: coordinate_
            }
        });
        this.cached_garden[coordinate_[0]][coordinate_[1]] = { id: item_id, growth: growth_ || 0 };
    }
    async unplant(coordinate_) {
        var e = await db.models.garden.findAll({
            where: {
                uid: this.id,
                coordinate: coordinate_
            },
            defaults: {
                uid: this.id,
                coordinate: coordinate_
            }
        });
        if (e.length) await db.models.garden.destroy({
            where: {
                uid: this.id,
                coordinate: coordinate_
            }
        });
        delete this.cached_garden[coordinate[0]][coordinate[1]];
    }
    async set_item(id, amount) {
        await db.models.inventory.findOrCreate({
            where: {
                uid: this.id,
                item_name: id
            },
            defaults: {
                uid: this.id,
                item_name: id
            }
        });
        await db.models.inventory.update({
            amount: amount
        }, {
            where: {
                uid: this.id,
                item_name: id
            }
        });
        this.cached_inventory[id] = amount;
    }
    async set_balance(amount) {
        await db.models.user.findOrCreate({
            where: {
                uid: this.id
            }
        });
        await db.models.user.update({
            balance: parseInt(amount)
        }, {
            where: {
                uid: this.id
            }
        });
    }
    async get_item_amount(id) {
        var inv = await this.get_inventory();
        return inv && inv[id] ? inv[id] : 0;
    }
    async get_balance() {
        var user = await db.models.user.findOrCreate({
            where: {
                uid: this.id
            },
            defaults: {
                uid: this.id
            }
        });
        user = user[0];
        return user.dataValues && user.dataValues.balance ? user.dataValues.balance : 0;
    }
    async get_corruption() {
        var user = await db.models.user.findOrCreate({
            where: {
                uid: this.id
            },
            defaults: {
                uid: this.id
            }
        });
        user = user[0];
        return user.dataValues && user.dataValues.corruption ? user.dataValues.corruption : 0;
    }
    async increment_corruption(amount) {
        var cur = await this.get_corruption();
        await db.models.user.update({
            corruption: cur + parseInt(amount)
        }, {
            where: {
                uid: this.id
            }
        });
    }
}
ctx.inventory._get_inventory = async function(user) {
    var invs = ctx.inventory._inventories.filter(inv => {
        return inv.id === user.id;
    });
    var single_inventory = Array.isArray(invs) ? (invs.length > 0 ? invs[0] : undefined) : invs;
    return single_inventory || new Inventory(user);
};
module.exports = [{
    'event': 'command',
    'name': 'inv_test',
    'description': 'superdev only',
    'permission': 3,
    'execute': async function(ctx, args, msg) {
        var inv = await ctx.inventory._get_inventory(msg.author);
        var item_amt = await inv.get_item_amount('test');
        msg.channel.createMessage("Test item . . . " + item_amt);
        inv.set_item('test', item_amt + 1);
    }
}, {
    'event': 'command',
    'name': 'bal_test',
    'description': 'superdev only',
    'permission': 3,
    'execute': async function(ctx, args, msg) {
        var inv = await ctx.inventory._get_inventory(msg.author);
        var item_amt = await inv.get_balance();
        msg.channel.createMessage("Test bal . . . " + item_amt);
        inv.set_balance(item_amt + 500000);
    }
}]
Users_Schema.sync();
Stats_Schema.sync();
Inventory_Schema.sync();