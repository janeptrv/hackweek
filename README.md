# Zen Garden

## a bot created for Discord's Hack Week 2019

Create, decorate and maintain your own zen garden! Achieve your ultimate zen state from the comfort of your own Discord client.

# running
`npm install`
`node bot.js`
you'll need to create a config.json with
- a bot token
- sql username and password

also postgres.